//
//  DBObject.m
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "DBObject.h"


@implementation DBObject

@dynamic dbID;
@dynamic create_date;
@dynamic modified;
@dynamic uploaded;
@dynamic tobeDeleted;


-(void) initValues{
    self.dbID = [NSUUID UUID].UUIDString;
    self.create_date = [NSDate date];
    self.modified = [NSDate date];
}

@end

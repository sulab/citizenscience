//
//  ASBindListVC.m
//  annotator
//
//  Created by Richard Good on 11/9/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "ASBindListVC.h"
#import "ASCoreData.h"
#import "GTConstants.h"
#import "ASBindEditorVC.h"

@interface ASBindListVC (){
    NSMutableArray * binds;
}


@end

@implementation ASBindListVC

-(void) insertNewObject:(id)sender{
    [self performSegueWithIdentifier:@"showBindDetailSegue" sender:self];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showBindDetailSegue"]){
        ASBindEditorVC* editor = (ASBindEditorVC*) segue.destinationViewController;
        if (sender==self){
            
            editor.bind =(ASBind*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:BindEntityName];
        }
        else{
            editor.Bind= (ASBind*)[binds objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        }
    }
}


-(void) computeBinds{
    if (binds){
        [binds removeAllObjects];
    }
    else{
        binds = [[NSMutableArray alloc]init];
    }
    
    NSSortDescriptor *sortName = [[NSSortDescriptor alloc] initWithKey:@"name"                                                                 ascending:NO];
    NSArray *sortKeys = [[NSArray alloc] initWithObjects:sortName, nil];
    
    [binds addObjectsFromArray:[[ASCoreData sharedInstance] entitiesofType:BindEntityName
                                                                  filteredBy:nil
                                                                    sortedBy:sortKeys
                                                                   ascending:YES
                                                                       inMoc:nil]] ;
    [self.tableView reloadData];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self computeBinds];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [binds count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"bindCell";
    ASBind * aBind = (ASBind *) binds[indexPath.row];
    
    UITableViewCell *cell = (UITableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    // Set up the cell...
    // Configure the cell...
    cell.textLabel.text = [NSString stringWithFormat:@"%@ to %@",aBind.sourceCategory.name,aBind.destinationCategory.name] ;
    cell.detailTextLabel.text = aBind.name;
    return (UITableViewCell*)cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

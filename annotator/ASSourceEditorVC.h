//
//  ASSourceEditorVC.h
//  annotator
//
//  Created by Richard Good on 11/9/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASSource.h"

@interface ASSourceEditorVC : UIViewController <UITextFieldDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *theURL;
@property (weak, nonatomic) IBOutlet UITextView *theContent;
@property (strong,nonatomic)    ASSource* source;
- (IBAction)useSource:(UIButton *)sender;

@end

//
//  ASSourceEditorVC.m
//  annotator
//
//  Created by Richard Good on 11/9/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "ASSourceEditorVC.h"
#import "ASCoreData.h"
#import "ASMarkup.h"
#import "ASUser.h"
#import "GTConstants.h"
#import "AnnotatorAD.h"

@interface ASSourceEditorVC (){
    AnnotatorAD * annotatorAD;
}

@end

@implementation ASSourceEditorVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.theContent.text = self.source.content;
    self.theURL.text = self.source.url;
    annotatorAD = [[UIApplication sharedApplication]delegate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.source.content = self.theContent.text ;
    self.source.url = self.theURL.text;
    [[ASCoreData sharedInstance]saveContext];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)useSource:(UIButton *)sender {
    
    ASMarkup * markup = (ASMarkup *)[[ASCoreData sharedInstance] getMarkupFor:self.source.url byUser:annotatorAD.currentUser.name];
    if (!markup){
        ASMarkup * markup = (ASMarkup *)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:MarkupEntityname];
        markup.enteredBy = annotatorAD.currentUser;
        markup.isFor =self.source;
        markup.content = self.source.content;
        [[ASCoreData sharedInstance]saveContext];
    }
    annotatorAD.currentSource = self.source;
    annotatorAD.currentMarkup = markup;
}
@end

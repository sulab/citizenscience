//
//  ASUser.m
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "ASUser.h"


@implementation ASUser

@dynamic name;
@dynamic kind;
@dynamic hasMarkups;

@end

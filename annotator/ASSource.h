//
//  ASSource.h
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DbObject.h"

@class ASMarkup;

@interface ASSource : DBObject

@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSSet *hasMarkups;
@end

@interface ASSource (CoreDataGeneratedAccessors)

- (void)addHasMarkupsObject:(ASMarkup *)value;
- (void)removeHasMarkupsObject:(ASMarkup *)value;
- (void)addHasMarkups:(NSSet *)values;
- (void)removeHasMarkups:(NSSet *)values;

@end

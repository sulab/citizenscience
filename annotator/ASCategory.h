//
//  ASCategory.h
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DBObject.h"


@interface ASCategory : DBObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *isSourceFor;
@property (nonatomic, retain) NSSet *isDestinationFor;
@property (nonatomic, retain) NSSet *superClassOf;
@end

@interface ASCategory (CoreDataGeneratedAccessors)

- (void)addIsSourceForObject:(NSManagedObject *)value;
- (void)removeIsSourceForObject:(NSManagedObject *)value;
- (void)addIsSourceFor:(NSSet *)values;
- (void)removeIsSourceFor:(NSSet *)values;

- (void)addIsDestinationForObject:(NSManagedObject *)value;
- (void)removeIsDestinationForObject:(NSManagedObject *)value;
- (void)addIsDestinationFor:(NSSet *)values;
- (void)removeIsDestinationFor:(NSSet *)values;

- (void)addSuperClassOfObject:(NSManagedObject *)value;
- (void)removeSuperClassOfObject:(NSManagedObject *)value;
- (void)addSuperClassOf:(NSSet *)values;
- (void)removeSuperClassOf:(NSSet *)values;

@end

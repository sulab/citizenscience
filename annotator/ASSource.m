//
//  ASSource.m
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "ASSource.h"
#import "ASMarkup.h"


@implementation ASSource

@dynamic url;
@dynamic content;
@dynamic hasMarkups;

@end

//
//  ASBindEditorVC.m
//  annotator
//
//  Created by Richard Good on 11/9/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "ASBindEditorVC.h"
#import "ASCategory.h"
#import "ASCoreData.h"

@interface ASBindEditorVC (){
    ASCategory  *geneCategory;
    ASCategory  *drugCategory;
    ASCategory  *diseaseCategory;
}

@end

@implementation ASBindEditorVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([self.bind.sourceCategory.name isEqualToString:@"Gene"])
        self.sourceCategory.selectedSegmentIndex =0;
    else     if ([self.bind.sourceCategory.name isEqualToString:@"Drug"])
        self.sourceCategory.selectedSegmentIndex =1;
    else     if ([self.bind.sourceCategory.name isEqualToString:@"Disease"])
        self.sourceCategory.selectedSegmentIndex =2;
 
    if ([self.bind.destinationCategory.name isEqualToString:@"Gene"])
        self.destinationCategory.selectedSegmentIndex =0;
    else     if ([self.bind.destinationCategory.name isEqualToString:@"Drug"])
        self.destinationCategory.selectedSegmentIndex =1;
    else     if ([self.bind.destinationCategory.name isEqualToString:@"Disease"])
        self.destinationCategory.selectedSegmentIndex =2;
    
    self.bindName.text = self.bind.name;
    
    geneCategory =[[ASCoreData sharedInstance] categoryByName:@"Gene"];
    drugCategory =[[ASCoreData sharedInstance] categoryByName:@"Drug"];
   diseaseCategory=  [[ASCoreData sharedInstance] categoryByName:@"Disease"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    switch (self.sourceCategory.selectedSegmentIndex) {
        case 0:
            self.bind.sourceCategory = geneCategory;
            break;
        case 1:
            self.bind.sourceCategory = drugCategory;
            break;
        case 2:
            self.bind.sourceCategory = diseaseCategory;
            break;
          
        default:
            break;
    }
    
    switch (self.destinationCategory.selectedSegmentIndex) {
        case 0:
            self.bind.destinationCategory = geneCategory;
            break;
        case 1:
            self.bind.destinationCategory = drugCategory;
            break;
        case 2:
            self.bind.destinationCategory = diseaseCategory;
            break;
            
        default:
            break;
    }
    
    self.bind.name = self.bindName.text;
    [[ASCoreData sharedInstance]saveContext];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

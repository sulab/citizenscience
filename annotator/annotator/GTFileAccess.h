//
//  GTFileAccess.h
//  trueeight
//
//  Created by Richard Good on 8/11/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTFileAccess : NSObject
+ (GTFileAccess *)sharedInstance;

+ (NSString *)applicationDocumentsDirectory;
+ (NSString*) buildFilepath:(NSString*) entityName requestType:(NSString*) rType;
+ (NSString*) buildImageFilepath:(NSString*) imageID;
+ (NSString*) buildTempImageFilepath:(NSString*) imageID;
+ (BOOL) imagefileforIDExists:(NSString*) imageID;
+ (void) deleteFileAtPath:(NSString*)fullpath;
+ (void) deleteAllImageFiles;
+ (void) deleteAllTempFiles;
+ (void) storeDefaultsFor:(NSString*)defaultBundleName withPlistName:(NSString*)plistName;
+ (NSArray*)findFilesinInbox;
+ (void)deleteAllFilesinInbox;

+(void)writeStr:(NSString*)data
     tofilepath:(NSString*)filepath;
@end

//
//  ASAnnotationText.h
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol conceptCategory <NSObject>
-(void)insertAConcept:(NSString*)content forKind:(NSString*)kind atLocation:( NSRange)location;
@end

@interface ASAnnotationText : UITextView

@property (nonatomic, weak) id <conceptCategory> conceptDelegate;


@end

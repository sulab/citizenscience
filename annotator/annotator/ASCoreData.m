//
//  ASCoreData.m
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "ASCoreData.h"
#import "GTFileAccess.h"
#import "GTConstants.h"
#import "NSDate+Normalize.h"


//
//  GTCoreData.m
//  trueeight
//
//  Created by Richard Good on 8/11/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//


NSString * const kCoredataFileUTI = @"com.trueidapps.tardydb";
NSString * const kFileLocation = @"kFileLocation";
NSString * const kSQLiteKey = @"kSQLiteKey";


@implementation ASCoreData
@synthesize dbStoreLocation;


// Core Data
// Abstract methods

+ (ASCoreData *)sharedInstance
{
    static ASCoreData *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ASCoreData alloc] init];
        // Do any other initialisation stuff here
    });
    
    return sharedInstance;
}


-(NSManagedObjectContext*)getValidMOC:(NSManagedObjectContext*)possibleMOC{
    if (possibleMOC)
        return possibleMOC;
    else
        return  self.managedObjectContext;
}

-(NSManagedObjectContext*) newMOC {
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [moc setParentContext:self.managedObjectContext];
    return moc;
}


/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext {
    
    if (managedObjectContext_ != nil) {
        return managedObjectContext_;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    
    if (coordinator != nil) {
        managedObjectContext_ = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [managedObjectContext_ setPersistentStoreCoordinator: coordinator];
    }
    
    
    [managedObjectContext_ setMergePolicy:[[NSMergePolicy alloc]initWithMergeType: NSMergeByPropertyObjectTrumpMergePolicyType]];
    return managedObjectContext_;
}


/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel {
    
    if (managedObjectModel_ != nil) {
        return managedObjectModel_;
    }
    managedObjectModel_ = [NSManagedObjectModel mergedModelFromBundles:nil];
    return managedObjectModel_;
}


/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    NSError *error=nil;
    if (persistentStoreCoordinator_ != nil) {
        return persistentStoreCoordinator_;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[GTFileAccess applicationDocumentsDirectory] stringByAppendingPathComponent: dbStoreLocation]];
    
    
    persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    
    NSMutableDictionary *options = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                                    [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption,
                                    nil];
    
    NSDictionary *configOptions = @{ NSSQLitePragmasOption : @{@"journal_mode" : @"DELETE"} };
    [options addEntriesFromDictionary:configOptions];
    
    
    if (![persistentStoreCoordinator_ addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) {
        // Handle error
        NSLog(@"Error creating persistent store.\n%@",error);
    }
    
    return persistentStoreCoordinator_;
}


-(void) resetPersistentStoreCoordinator:(NSTimer*) timer{
    //	NSFileManager *fileManager = [NSFileManager defaultManager];
    //	if  (![fileManager fileExistsAtPath:[[EGFileAccess applicationDocumentsDirectory] stringByAppendingPathComponent: dbStoreLocation]]){
    //		[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(resetPersistentStoreCoordiantor:) userInfo:nil repeats:NO];
    //	}
    //	else {
    if (managedObjectModel_)	{
        managedObjectModel_ =nil;
    }
    [self managedObjectModel];
    if (persistentStoreCoordinator_)	{
        persistentStoreCoordinator_ =nil;
    }
    [self persistentStoreCoordinator];
    [self resetMOC];
    //	}
    
    
}

- (void)saveContext {
    //   NSLog(@"Saving overall context");
    
    NSError *error = nil;
    if (managedObjectContext_ != nil) {
        if ([managedObjectContext_ hasChanges]){
            if ( ![managedObjectContext_ save:&error]) {
                /*
                 Replace this implementation with code to handle the error appropriately.
                 
                 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
                 */
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [[NSNotificationCenter defaultCenter] postNotificationName: @"databaseSaveComplete" object:nil userInfo:nil];
                });
                
            }
        }
        else{
            NSLog(@"SAVING parent MOC WITH NO CHANGES.");
        }
        
    }
}


- (void)saveContext:(NSManagedObjectContext*)moc{
    //    NSLog(@"Saving child context");
    NSError *error = nil;
    if (moc != nil) {
        
        //       if ([moc hasChanges]) {
        if (![moc save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        //        }
        //        else{
        //           NSLog(@"SAVING Child MOC WITH NO CHANGES.");
        //
        //        }
    }
    
    
}

- (void) resetMOC {
    if (managedObjectContext_)	{
        managedObjectContext_ =nil;
    }
    
}


- (void) resetMOM {
    if (managedObjectModel_)	{
        managedObjectModel_ =nil;
    }
}



-(void) removeAllEntitiesofType:(NSString*) entityName {
    
    @autoreleasepool {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        fetchRequest.resultType = NSManagedObjectIDResultType;
        __autoreleasing NSError *error = nil;
        NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        if (fetchedObjects == nil) {
            if ([self.delegate respondsToSelector:@selector(handleErrorMessage:)])
                [self.delegate handleErrorMessage:[NSString stringWithFormat:@"Error clearing db for %@.",entityName]];
        }
        else{
            // first get the ids into a separate array
            NSSet* objectsToDelete = [[NSSet alloc]initWithArray:fetchedObjects];
            for (NSManagedObjectID* object in objectsToDelete){
                NSManagedObject* dObject = [self.managedObjectContext objectWithID:object];
                [self.managedObjectContext deleteObject:dObject];
            }
            [self saveContext:self.managedObjectContext];
        }
    }
}

-(void) listAllEntitiesofType:(NSString*) entityName inMOC:(NSManagedObjectContext*)moc
{
    NSManagedObjectContext   *currentMOC = [self getValidMOC:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:currentMOC];
    [fetchRequest setEntity:entity];
    
    __autoreleasing NSError *error = nil;
    NSArray *fetchedObjects = [currentMOC executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        if ([self.delegate respondsToSelector:@selector(handleErrorMessage:)])
            [self.delegate handleErrorMessage:[NSString stringWithFormat:@"Error listing db for %@.",entityName]];
    }
    else{
        int count=0;
        for (NSManagedObject* object in fetchedObjects) {
            NSLog(@"%d %@ [\n%@\n]",count,entityName,object);
        }
    }
}

-(NSInteger) countAllEntitiesofType:(NSString*) entityName
                              inMOC:(NSManagedObjectContext*)moc{
    NSManagedObjectContext   *currentMOC = [self getValidMOC:moc];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSError *error = nil;
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:currentMOC];
    [fetchRequest setEntity:entity];
    return [currentMOC countForFetchRequest:fetchRequest error:&error];
}

-(NSInteger) countAllModifedEntitiesofType:(NSString*) entityName
                                     inMOC:(NSManagedObjectContext*)moc{
    
    NSManagedObjectContext   *currentMOC = [self getValidMOC:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSError *error = nil;
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"modified != create_date"]];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:currentMOC];
    [fetchRequest setEntity:entity];
    return [currentMOC countForFetchRequest:fetchRequest error:&error];
}

-(NSInteger) uploadCount:(NSString*) entityName inMOC:(NSManagedObjectContext*)moc{
    NSManagedObjectContext   *currentMOC = [self getValidMOC:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSError *error = nil;
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"uploaded==NO"]];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:currentMOC];
    [fetchRequest setEntity:entity];
    return [currentMOC countForFetchRequest:fetchRequest error:&error];
}




- (void) resetDatabase {
    [self resetMOC];
    [self resetMOM];
    if ([self.delegate respondsToSelector:@selector(dbClearComplete)])
        [self.delegate dbClearComplete];
}


- (void) clearDatabase{
    [self deleteTheStore:nil];
}



-(void) deleteTheStore:(NSTimer*)timer{
    NSError* error;
    NSString* storeFilepath =  [[GTFileAccess applicationDocumentsDirectory] stringByAppendingPathComponent: dbStoreLocation];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:storeFilepath]){
        [fileManager removeItemAtPath:storeFilepath error:&error];
        if  ([fileManager fileExistsAtPath:storeFilepath] && !error){
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(deleteTheStore:) userInfo:nil repeats:NO];
        }
        else if (error){
            NSLog(@"Error deleteing store %@",error);
        }
        
    }
    managedObjectContext_=nil;
    managedObjectModel_=nil;
    persistentStoreCoordinator_=nil;
    [self managedObjectContext];
    
}


-(BOOL) deleteRequired {
    NSString* storeFilepath =  [[GTFileAccess applicationDocumentsDirectory] stringByAppendingPathComponent: dbStoreLocation];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:storeFilepath]){
        NSPersistentStoreCoordinator *psc = [self persistentStoreCoordinator];
        NSString *sourceStoreType =  NSSQLiteStoreType;
        NSURL *sourceStoreURL = [NSURL fileURLWithPath: [[GTFileAccess applicationDocumentsDirectory] stringByAppendingPathComponent: dbStoreLocation]];
        NSError *error = nil;
        
        NSDictionary *sourceMetadata =
        [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:sourceStoreType
                                                                   URL:sourceStoreURL
                                                                 error:&error];
        
        if (sourceMetadata == nil) {
            NSLog(@"Error checking on store version.");
        }
        
        NSString *configuration = nil ;
        NSManagedObjectModel *destinationModel = [psc managedObjectModel];
        BOOL pscCompatibile = [destinationModel
                               isConfiguration:configuration
                               compatibleWithStoreMetadata:sourceMetadata];
        
        if (!pscCompatibile) {
            // Deal with error...
            [fileManager removeItemAtPath:storeFilepath error:NULL];
            [GTFileAccess deleteAllTempFiles];
            
            return YES;
        }
        return NO;
    }
    return NO;
}


-(NSArray*) entitiesofType:(NSString*)entType
                filteredBy:(NSPredicate*)filter
                  sortedBy:(NSArray*)sortKeys
                 ascending:(BOOL)direction
                     inMoc:(NSManagedObjectContext*)localMOC{
    
    NSManagedObjectContext   *currentMOC = [self getValidMOC:localMOC];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entType inManagedObjectContext:currentMOC];
    [request setEntity:entity];
    
    if (sortKeys!=nil && [sortKeys count]>0)	{
        [request setSortDescriptors:sortKeys];
    }
    if (filter){
        [request setPredicate:filter];
    }
    NSError* error=nil;
    NSArray* fetchedObjects = [currentMOC executeFetchRequest:request error:&error];
    if (!error && fetchedObjects)
        return fetchedObjects;
    else
        return nil;
}



-(NSMutableArray*) copyAllEntitiesofType:(NSString*)entType inMOC:(NSManagedObjectContext*)moc sortedBy:(NSString*)sortKey ascending:(BOOL)direction	{
    NSManagedObjectContext   *currentMOC = [self getValidMOC:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entType inManagedObjectContext:currentMOC];
    [request setEntity:entity];
    
    if (sortKey!=nil)	{
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:direction];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        [request setSortDescriptors:sortDescriptors];
    }
    NSError* error=nil;
    NSArray* fetchedObjects = [currentMOC executeFetchRequest:request error:&error];
    if (!error && fetchedObjects)
        return [[NSMutableArray alloc]initWithArray:fetchedObjects];
    else
        return nil;
}

- (NSMutableArray*) copyAllEntitiesofType:(NSString*)entType sortedBy:(NSString*)sortKey ascending:(BOOL)direction{
    return [self copyAllEntitiesofType:entType inMOC:self.managedObjectContext sortedBy:sortKey ascending:direction];
}

#pragma mark Creates



#pragma mark  Queries

-(ASCategory*) categoryByName:(NSString*)target{
    NSError *error;
    NSArray *fetchedObjects;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    // School
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:CategoryEntityname inManagedObjectContext:self.managedObjectContext];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(name == %@)", target];
    [fetchRequest setEntity:entityDesc];
    [fetchRequest setPredicate:predicate];
    fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if ([fetchedObjects count]==1)
        return (ASCategory*) [fetchedObjects objectAtIndex:0];
    
    else if ([fetchedObjects count]>1) {
        NSLog(@"Multiple ASCategory with:%@",target);
        return (ASCategory*) [fetchedObjects objectAtIndex:0];
    }
    return nil;
  
}


-(ASSource*)  getSourceByURL:(NSString*)target{
    NSError *error;
    NSArray *fetchedObjects;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    // School
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:SourceEntityname inManagedObjectContext:self.managedObjectContext];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(url == %@)", target];
    [fetchRequest setEntity:entityDesc];
    [fetchRequest setPredicate:predicate];
    fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if ([fetchedObjects count]==1)
        return (ASSource*) [fetchedObjects objectAtIndex:0];
    
    else if ([fetchedObjects count]>1) {
        NSLog(@"Multiple source with:%@",target);
        return (ASSource*) [fetchedObjects objectAtIndex:0];
    }
    return nil;
   
}
-(ASUser*)   getUserByName:(NSString*)target{
    NSError *error;
    NSArray *fetchedObjects;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    // School
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:UserEntityname inManagedObjectContext:self.managedObjectContext];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(name == %@)", target];
    [fetchRequest setEntity:entityDesc];
    [fetchRequest setPredicate:predicate];
    fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if ([fetchedObjects count]==1)
        return (ASUser*) [fetchedObjects objectAtIndex:0];
    
    else if ([fetchedObjects count]>1) {
        NSLog(@"Multiple  with name:%@",target);
        return (ASUser*) [fetchedObjects objectAtIndex:0];
    }
    return nil;
   
}

-(ASMarkup*)   getMarkupFor:(NSString*)sourceURL byUser:(NSString*)userName{
    NSError *error;
    NSArray *fetchedObjects;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    // School
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:MarkupEntityname inManagedObjectContext:self.managedObjectContext];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(enteredBy.name == %@) and (isFor.url==%@)", userName,sourceURL];
    [fetchRequest setEntity:entityDesc];
    [fetchRequest setPredicate:predicate];
    fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if ([fetchedObjects count]==1)
        return (ASMarkup*) [fetchedObjects objectAtIndex:0];
    
    else if ([fetchedObjects count]>1) {
        NSLog(@"Multiple  with sourceURL:%@",sourceURL);
        return (ASMarkup*) [fetchedObjects objectAtIndex:0];
    }
    return nil;
    
}



-(DBObject*)findEntityByUUID:(NSString*) targetUUID{
    if (!targetUUID) return nil;
    NSError *error;
    NSArray *fetchedObjects;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    // School
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:DBObjectEntityname inManagedObjectContext:self.managedObjectContext];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(dbID == %@)", targetUUID];
    [fetchRequest setEntity:entityDesc];
    [fetchRequest setPredicate:predicate];
    fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if ([fetchedObjects count]==1)
        return (DBObject*) [fetchedObjects objectAtIndex:0];
    
    else if ([fetchedObjects count]>1) {
        NSLog(@"Multiple students with UUID:%@",targetUUID);
        return (DBObject*) [fetchedObjects objectAtIndex:0];
    }
    return nil;
}

-(NSArray*)findEntitiesByUpdateFlag:(BOOL) updateStatus
                             ofKind:(NSString*)entityName
                              inMOC:(NSManagedObjectContext*)localMOC{
    NSManagedObjectContext   *currentMOC = [self getValidMOC:localMOC];
    NSError *error;
    NSArray *fetchedObjects;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    // School
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:entityName inManagedObjectContext:currentMOC];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(update_flag==%d)", updateStatus];
    [fetchRequest setEntity:entityDesc];
    [fetchRequest setPredicate:predicate];
    fetchedObjects = [currentMOC executeFetchRequest:fetchRequest error:&error];
    return  fetchedObjects;
}



-(DBObject*)findorCreateEntityByUUID:(NSString*) targetUUID
                                ofType:(NSString*)entityType{
    DBObject* target = [self findEntityByUUID:targetUUID];
    if (target)
        return target;
    else	{	// create and return
        NSEntityDescription *entityDesc = [NSEntityDescription entityForName:entityType inManagedObjectContext:self.managedObjectContext];
        DBObject* aEntity = (DBObject*) [[NSManagedObject alloc] initWithEntity:entityDesc insertIntoManagedObjectContext:self.managedObjectContext];
        [aEntity initValues];
        if (targetUUID)
            aEntity.dbID =targetUUID;
        return aEntity;
    }
}



-(NSArray*)findDBObjectsOfKind:(NSString*)dbObjectKind
                      starting:(NSDate*)startDate
                        ending:(NSDate*)endDate
                          sort:(NSArray*)sortDescriptors{
    NSPredicate *filter=nil;
    if (startDate && endDate)
        filter = [NSPredicate predicateWithFormat:@"(created_on >= %@) AND (created_on <= %@)", startDate, endDate];
    else if (startDate)
        filter = [NSPredicate predicateWithFormat:@"(created_on >= %@) ", startDate];
    else if (endDate)
        filter = [NSPredicate predicateWithFormat:@"(created_on <= %@) ", endDate];
    
    
    return [self entitiesofType:dbObjectKind
                     filteredBy:filter
                       sortedBy:sortDescriptors
                      ascending:YES
                          inMoc:nil];
    
}


#pragma mark Utils

-(void)removeDemodata{
    @autoreleasepool {
//        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//        NSEntityDescription *entity = [NSEntityDescription entityForName:StudentEntityname inManagedObjectContext:self.managedObjectContext];
//        [fetchRequest setEntity:entity];
//        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"unique_id BEGINSWITH %@ ",@"TIDHigh"]];
//        fetchRequest.resultType = NSManagedObjectIDResultType;
//        __autoreleasing NSError *error = nil;
//        NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
//        if (fetchedObjects == nil) {
//            if ([self.delegate respondsToSelector:@selector(handleErrorMessage:)])
//                [self.delegate handleErrorMessage:[NSString stringWithFormat:@"Error clearing db for demo data"]];
//        }
//        else{
//            // first get the ids into a separate array
//            NSSet* objectsToDelete = [[NSSet alloc]initWithArray:fetchedObjects];
//            for (NSManagedObjectID* object in objectsToDelete){
//                NSManagedObject* dObject = [self.managedObjectContext objectWithID:object];
//                [self.managedObjectContext deleteObject:dObject];
//            }
//            [self saveContext:self.managedObjectContext];
//        }
    }
}


-(NSURL*)getDBStoreasURL{
    return [NSURL fileURLWithPath: [[GTFileAccess applicationDocumentsDirectory] stringByAppendingPathComponent: [ASCoreData sharedInstance].dbStoreLocation]];
    
}

-(void) replaceDBStoreWithData:(NSData*)dbContent{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString* storeFilepath = [[GTFileAccess applicationDocumentsDirectory] stringByAppendingPathComponent: self.dbStoreLocation];
    NSError* error;
    if ([fileManager fileExistsAtPath:storeFilepath]){
        [fileManager removeItemAtPath:storeFilepath error:&error];
        if (error){
            NSLog(@"Error deleteing store %@",error);
        }
    }
    if (!error){
        [dbContent writeToFile:storeFilepath atomically:YES];
        managedObjectContext_=nil;
        managedObjectModel_=nil;
        persistentStoreCoordinator_=nil;
        [self managedObjectContext];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [[NSNotificationCenter defaultCenter] postNotificationName: @"databaseImportComplete" object:nil userInfo:nil];
        });
        
    }
}







-(void) deleteDBObject:(DBObject*)deleteTarget{
    NSManagedObjectContext  * targetMOC =deleteTarget.managedObjectContext;
    [targetMOC deleteObject:deleteTarget];
    [self saveContext:targetMOC];
    [self saveContext];
    
}



@end

//
//  NSDate+Normalize.m
//  PainFreeMenus
//
//  Created by Richard Good on 3/11/14.
//  Copyright (c) 2014 TrueIDApps. All rights reserved.
//

#import "NSDate+Normalize.h"

@implementation NSDate (Normalize)

-(NSDate*)normalizeToHour{
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay |NSCalendarUnitHour) fromDate:self];
    return  [gregorian dateFromComponents:dateComponents];
}

-(NSDate*)normalizeToDay{
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:self];
    return  [gregorian dateFromComponents:dateComponents];
}

-(NSDate*)normalizeToToday{
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:self];
    NSDateComponents *todayComponents = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    dateComponents.year =todayComponents.year;
    dateComponents.month =todayComponents.month;
    dateComponents.day =todayComponents.day;
   
    return  [gregorian dateFromComponents:dateComponents];

    
}

@end

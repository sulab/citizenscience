//
//  GTFileAccess.m
//  trueeight
//
//  Created by Richard Good on 8/11/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "GTFileAccess.h"

@implementation GTFileAccess
+ (GTFileAccess *)sharedInstance
{
    static GTFileAccess *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[GTFileAccess alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

+(void) deleteAllTempFiles	{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dataDirectory = [NSString stringWithFormat:@"%@/Data",[paths objectAtIndex:0]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError* error;
    NSArray * temporaryFiles = 	[fileManager contentsOfDirectoryAtPath:dataDirectory error:&error];
    for (NSString* filepath in temporaryFiles)	{
        //		NSLog(@"%@",[NSString stringWithFormat:@"%@/%@",dataDirectory,filepath]);
        [self deleteFileAtPath:[NSString stringWithFormat:@"%@/%@",dataDirectory,filepath]];
        
    }
}

#pragma mark -
#pragma mark Application's documents directory

/**
 Returns the path to the application's documents directory.
 */
+ (NSString *)applicationDocumentsDirectory {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

+(void)deleteFileAtPath:(NSString*)fullpath{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:fullpath]){
        [fileManager removeItemAtPath:fullpath error:NULL];
    }
}

+ (NSString*) buildImageFilepath:(NSString*) imageID {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [NSString stringWithFormat:@"%@/Images/%@",documentsDirectory,imageID];
}

+ (NSString*) buildTempImageFilepath:(NSString*) imageID {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [NSString stringWithFormat:@"%@/TempImages/%@",documentsDirectory,imageID];
}


+ (NSString*) buildFilepath:(NSString*) entityName requestType:(NSString*) rType {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    if ([rType isEqualToString:@"Entity"])
        return [NSString stringWithFormat:@"%@/Data/%@.xml",documentsDirectory,entityName];
    else
        return [NSString stringWithFormat:@"%@/Data/Rel_%@.xml",documentsDirectory,entityName];
}


+ (BOOL) imagefileforIDExists:(NSString*) imageID {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:[self buildImageFilepath:imageID]];
}

+(void) deleteAllImageFiles{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError*	error;
    NSArray* imageFiles = [fileManager contentsOfDirectoryAtPath:[self buildImageFilepath:@""] error:&error];
    for (NSString* filename in imageFiles){
        if ([fileManager fileExistsAtPath:[self buildImageFilepath:filename]]){
            [fileManager removeItemAtPath:[self buildImageFilepath:filename] error:NULL];
        }
    }
}


+(BOOL) tempFilesPresent	{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dataDirectory = [NSString stringWithFormat:@"%@/Data",[paths objectAtIndex:0]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError* error;
    NSArray * temporaryFiles = 	[fileManager contentsOfDirectoryAtPath:dataDirectory error:&error];
    BOOL tempFound = NO;
    for (NSString* filepath in temporaryFiles) {
        tempFound = [filepath hasSuffix:@"xml"] || tempFound;
    }
    return tempFound;
}

+(void)storeDefaultsFor:(NSString*)defaultBundleName withPlistName:(NSString*)plistName{
    //
    NSString *pathStr = [[NSBundle mainBundle] bundlePath];
    NSString *settingsBundlePath = [pathStr stringByAppendingPathComponent:defaultBundleName];
    NSString *finalPath = [settingsBundlePath stringByAppendingPathComponent:plistName];
    
    NSDictionary *settingsDict = [NSDictionary dictionaryWithContentsOfFile:finalPath];
    NSArray *prefSpecifierArray = [settingsDict objectForKey:@"PreferenceSpecifiers"];
    NSDictionary *prefItem;
    
    for (prefItem in prefSpecifierArray)
    {
        
        NSString *keyValueStr = [prefItem objectForKey:@"Key"];
        if (keyValueStr)	{
            id defaultValue = [prefItem objectForKey:@"DefaultValue"];
            [[NSUserDefaults standardUserDefaults] setObject:defaultValue forKey:keyValueStr];				
        }
    }
    
}

+(void)writeStr:(NSString*)data
     tofilepath:(NSString*)filepath{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError* error;
    if ([fileManager fileExistsAtPath:filepath]){
        [fileManager removeItemAtPath:filepath error:&error];
        if (error){
            NSLog(@"Error deleting %@  error:%@",filepath,error);
        }
    }
    if (!error){
        [data writeToFile:filepath
               atomically:YES
                 encoding:NSUTF8StringEncoding
                    error:&error];
    }
}

+(NSArray*)findFilesinInbox {
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* inboxPath = [documentsDirectory stringByAppendingPathComponent:@"Inbox"];
   return [filemgr contentsOfDirectoryAtPath:inboxPath error:nil];
}


+(void)deleteAllFilesinInbox{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* inboxPath = [documentsDirectory stringByAppendingPathComponent:@"Inbox"];
    NSArray* filenames= [fileManager contentsOfDirectoryAtPath:inboxPath error:nil];
    for (NSString* filename in filenames){
        NSError*	error;
        NSString    *deleteFilepath = [NSString stringWithFormat:@"%@/%@",inboxPath,filename];
        if ([fileManager fileExistsAtPath:deleteFilepath]){
            [fileManager removeItemAtPath:deleteFilepath error:&error];
        }
    }
       
    
}

@end

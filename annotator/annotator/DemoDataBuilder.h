//
//  DemoDataBuilder.h
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASUser.h"
#import "ASSource.h"

@interface DemoDataBuilder : NSObject

-(void) initData;
@end

//
//  ASConceptVC.m
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "ASConceptVC.h"
#import "ASCoreData.h"
#import "GTConstants.h"
#import "ASCategory.h"
#import "ASConcept.h"
#import "AnnotatorAD.h"
#import "ASConceptBind.h"
#import  "ASBind.h"

@interface ASConceptVC (){
    NSMutableArray * categories;
    NSMutableDictionary  * conceptData;
    AnnotatorAD * annotatorAD;
    ASBindsChooserTVC   * bindChooser;
    ASConcept   * sourceConcept;
    ASConcept   * destinationConcept;
    
    NSMutableArray  * binds;
}

@end

@implementation ASConceptVC

#pragma mark Copy


-(void)insertAConcept:(NSString*)content forKind:(NSString*)kind atLocation:(NSRange)location{
    ASConcept* aConcept = (ASConcept*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil
                                                   ofType:ConceptEntityname];
    ASCategory* category = [[ASCoreData sharedInstance] categoryByName:kind];
    aConcept.kindOf = category;
    aConcept.foundIn =annotatorAD.currentMarkup;
    aConcept.start_pos = [NSNumber numberWithUnsignedInteger: location.location];
    aConcept.length = [NSNumber numberWithUnsignedInteger: location.length];
    aConcept.name = content;
    [[ASCoreData sharedInstance]saveContext];
    [self computeConcepts];
    [self.concepts reloadData];    
}

-(void)computeConcepts{
    
    NSSortDescriptor *sortName = [[NSSortDescriptor alloc] initWithKey:@"name"                                                                 ascending:NO];
    NSArray *sortKeys = [[NSArray alloc] initWithObjects:sortName, nil];
    
    if (categories){
        // cleanup first
        for (ASCategory * aCategory in categories){
            [(NSMutableArray*)[conceptData objectForKey:aCategory.name] removeAllObjects];
        }
        [categories removeAllObjects];
    }
    else {
        categories = [[NSMutableArray alloc]init];
    }
    
    [categories addObjectsFromArray:[[ASCoreData sharedInstance]  entitiesofType:CategoryEntityname
                                                                      filteredBy:nil
                                                                        sortedBy:sortKeys
                                                                       ascending:YES
                                                                           inMoc:nil]];
    if (conceptData){
        [conceptData removeAllObjects];
    }
    else{
        conceptData = [[NSMutableDictionary alloc]init];
    }
    for (ASCategory * aCategory in categories){
        NSMutableArray  *conceptSet = [[NSMutableArray alloc]init];
        NSPredicate * identifiedConcepts = [NSPredicate predicateWithFormat:@"foundIn.dbID==%@ and  kindOf.name=%@",annotatorAD.currentMarkup.dbID, aCategory.name];
        [conceptSet addObjectsFromArray: [[ASCoreData sharedInstance]  entitiesofType:ConceptEntityname
                                                                           filteredBy:identifiedConcepts
                                                                             sortedBy:sortKeys
                                                                            ascending:YES
                                                                                inMoc:nil]];
        
        [conceptData setObject:conceptSet forKey:aCategory.name];
    }
    
   
 }

-(void)computeBinds{
    if (binds){
        [binds removeAllObjects];
    }
    else{
        binds = [[NSMutableArray alloc]init];
    }
    NSSortDescriptor *sortBind = [[NSSortDescriptor alloc] initWithKey:@"isA.name"                                                                 ascending:NO];
    NSArray *sortBindKeys = [[NSArray alloc] initWithObjects:sortBind, nil];
    
    
    NSPredicate * localBinds =[NSPredicate predicateWithFormat:@"sourceBind.foundIn.dbID==%@",annotatorAD.currentMarkup.dbID];
    [binds addObjectsFromArray:[[ASCoreData sharedInstance]  entitiesofType:ConceptBindEntityname
                                                                 filteredBy:localBinds
                                                                   sortedBy:sortBindKeys
                                                                  ascending:YES
                                                                      inMoc:nil]];
    
}

- (void)preferredContentSizeChanged:(NSNotification *)aNotification {
    self.sourceText.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    annotatorAD = [[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view.
    self.sourceText.conceptDelegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(preferredContentSizeChanged:)
                                                 name:UIContentSizeCategoryDidChangeNotification object:nil];

 }

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self computeConcepts];
    [self computeBinds];
    [self.concepts reloadData];
    self.sourceText.text = annotatorAD.currentMarkup.content;
    self.sourceURLL.text = annotatorAD.currentSource.url;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedBindChooser"]){
        bindChooser = (ASBindsChooserTVC*) segue.destinationViewController;
        bindChooser.delegate = self;
    }
}

-(void)addBind:(ASBind *)bind{
    ASConceptBind * conceptBind = (ASConceptBind*)[[ASCoreData sharedInstance]findorCreateEntityByUUID:nil ofType:ConceptBindEntityname];
    conceptBind.sourceBind = sourceConcept;
    conceptBind.destinationBind = destinationConcept;
    conceptBind.isA = bind;
    [[ASCoreData sharedInstance]saveContext];
    [self computeBinds];
    [self.concepts reloadData];
    sourceConcept = nil;
    destinationConcept = nil;
    self.sourceL.text =@"";
    self.destinationL.text =@"";
    self.sourceText.text = annotatorAD.currentMarkup.content;
    [bindChooser showPossibleBindsFor:@"" withDestination:@""];
}


- (IBAction)selectText:(UIBarButtonItem *)sender {
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [categories count]+1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section==[categories count])
        return @"Bindings";
    
    return ((ASCategory*)categories[section]).name;
}


-(NSArray*) getSectionData:(NSInteger)section{
    if (section==[categories count]){
        return binds;
    }
    else {
    NSString *nameKey = ((ASCategory*)categories[section]).name;
    return (NSArray*)[conceptData objectForKey:nameKey];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==[categories count]){
        return [binds count];
    }
    else
        return [[self getSectionData:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"conceptCell";
    UITableViewCell *cell = (UITableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (indexPath.section==[categories count]){
        ASConceptBind * conceptBind = (ASConceptBind*)binds[indexPath.row];
        cell.textLabel.text = conceptBind.isA.name;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ --> %@",conceptBind.sourceBind.name, conceptBind.destinationBind.name];
   }
    else {
    
    ASConcept * aConcept = (ASConcept *) [self getSectionData:indexPath.section][indexPath.row];
    
    
    // Configure the cell...
    
    // Set up the cell...
    // Configure the cell...
    cell.textLabel.text = aConcept.name;
        if ([aConcept.name isEqualToString:sourceConcept.name]){
            [cell.textLabel setTextColor:[UIColor greenColor]];
            cell.detailTextLabel.text =@"From:";
        }
        else if ([aConcept.name isEqualToString:destinationConcept.name]){
            [cell.textLabel setTextColor:[UIColor redColor]];
            cell.detailTextLabel.text =@"To:";
        }
        else {
            [cell.textLabel setTextColor:[UIColor blackColor]];
            cell.detailTextLabel.text =@"";
        }
        
        
     }
    return (UITableViewCell*)cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (indexPath.section==[categories count]){   //show binding
            ASConceptBind* bind =(ASConceptBind*)binds[indexPath.row];
            [[ASCoreData sharedInstance].managedObjectContext deleteObject:bind];
            [binds removeObject:bind];
        }
        else {
            sourceConcept = (ASConcept *) [self getSectionData:indexPath.section][indexPath.row];
            [[ASCoreData sharedInstance].managedObjectContext deleteObject:sourceConcept];
            NSMutableArray*  conceptArray = (NSMutableArray*)[self getSectionData:indexPath.section];
            [conceptArray removeObject:sourceConcept];
            [self computeBinds];
            [self clearHighlighting];
       }
      }
    [tableView reloadData];
    
}

-(void) clearHighlighting{
   self.sourceText.text = annotatorAD.currentMarkup.content;
}

-(void) highlightWordInSource:(NSString*)word usingColor:(UIColor*)highlightColor{
    NSMutableAttributedString *mutable = [[NSMutableAttributedString alloc] initWithAttributedString:self.sourceText.attributedText];
    NSRange targetRange =[self.sourceText.text rangeOfString:word]; // first occurrence
    
    while (targetRange.location != NSNotFound){
        [mutable addAttribute: NSForegroundColorAttributeName value:highlightColor range:targetRange];
        targetRange.location = targetRange.location+targetRange.length;
        targetRange.length = [self.sourceText.text length] -  targetRange.location;
        targetRange =[self.sourceText.text rangeOfString:word options:0 range:targetRange];
    }
    self.sourceText.attributedText = mutable;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==[categories count]){   //show binding
        ASConceptBind* bind =(ASConceptBind*)binds[indexPath.row];
        
        self.sourceL.text = [NSString stringWithFormat:@"From: %@",bind.sourceBind.name];
        self.destinationL.text = [NSString stringWithFormat:@"To: %@",bind.destinationBind.name];
        [self clearHighlighting];
        [self highlightWordInSource:bind.sourceBind.name usingColor:[UIColor redColor]];
        [self highlightWordInSource:bind.destinationBind.name usingColor:[UIColor greenColor]];
 
    }
    else if (!sourceConcept){
        sourceConcept = (ASConcept *) [self getSectionData:indexPath.section][indexPath.row];
        self.sourceL.text = [NSString stringWithFormat:@"From: %@",sourceConcept.name];
        [self highlightWordInSource:sourceConcept.name usingColor:[UIColor redColor]];
    }
    else if (!destinationConcept){
        destinationConcept = (ASConcept *) [self getSectionData:indexPath.section][indexPath.row];
        self.destinationL.text = [NSString stringWithFormat:@"To: %@",destinationConcept.name];
        [bindChooser showPossibleBindsFor:sourceConcept.kindOf.name withDestination:destinationConcept.kindOf.name];
        [self highlightWordInSource:destinationConcept.name usingColor:[UIColor greenColor]];
   }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView reloadRowsAtIndexPaths:@[indexPath]
                     withRowAnimation:NO];
}

- (IBAction)clearSelections:(UIButton *)sender {
    sourceConcept=nil;
    destinationConcept =nil;
    [self clearHighlighting];
    [self.concepts reloadData];
    [bindChooser showPossibleBindsFor:@"" withDestination:@""];   
}
@end

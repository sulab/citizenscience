//
//  NSDate+Normalize.h
//  PainFreeMenus
//
//  Created by Richard Good on 3/11/14.
//  Copyright (c) 2014 TrueIDApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Normalize)
-(NSDate*)normalizeToHour;
-(NSDate*)normalizeToDay;
-(NSDate*)normalizeToToday; // used to compare arbitary hour minutes like a period

@end

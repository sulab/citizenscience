//
//  ASConceptVC.h
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASAnnotationText.h"
#import "ASBindsChooserTVC.h"


@interface ASConceptVC : UIViewController <UITableViewDataSource,UITableViewDelegate,conceptCategory,BindProtocol>

@property (weak, nonatomic) IBOutlet UITableView *concepts;

@property (weak, nonatomic) IBOutlet ASAnnotationText *sourceText;

@property (weak, nonatomic) IBOutlet UITableView *bindsList;
- (IBAction)selectText:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UILabel *destinationL;
@property (weak, nonatomic) IBOutlet UILabel *sourceL;
@property (weak, nonatomic) IBOutlet UILabel *sourceURLL;

- (IBAction)clearSelections:(UIButton *)sender;

@end

//
//  AppDelegate.m
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "AnnotatorAD.h"
#import "GTFileAccess.h"
#import "ASCoreData.h"
#import "GTConstants.h"
#import "DemoDataBuilder.h"

@interface AnnotatorAD ()

@end

@implementation AnnotatorAD


-(id) init{
    self = [super init];
    if (self){
        [ASCoreData sharedInstance].dbStoreLocation = @"AnnotationData.sqlite";        
    }
    return self;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    if ([[ASCoreData sharedInstance]countAllEntitiesofType:UserEntityname inMOC:nil]<1){
        DemoDataBuilder * builder = [[DemoDataBuilder alloc]init];
        [builder initData];
    }
    self.currentSource = [[ASCoreData sharedInstance] getSourceByURL:@"Test 1"];
    self.currentUser =  [[ASCoreData sharedInstance] getUserByName:@"TestUser"];
    self.currentMarkup = [[ASCoreData sharedInstance]  getMarkupFor:self.currentSource.url byUser:self.currentUser.name];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[ASCoreData sharedInstance] saveContext];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [[ASCoreData sharedInstance] saveContext];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

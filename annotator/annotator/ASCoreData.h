//
//  ASCoreData.h
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DBObject.h"
#import "ASConcept.h"
#import "ASBind.h"
#import "ASCategory.h"
#import "ASConceptBind.h"
#import "ASMarkup.h"
#import "ASSource.h"
#import "DBObject.h"

@protocol ASCDProtocol <NSObject>

@optional
-(void) downLoadProgress:(float)progress;
-(void) dbClearComplete;
-(void) dbSaveComplete;
-(void) handleErrorMessage:(NSString*)message;
@end

@interface ASCoreData : NSObject  {// Core Data
    NSString*           dbStoreLocation;
@private
    NSManagedObjectContext*          managedObjectContext_;
    NSManagedObjectModel*            managedObjectModel_;
    NSPersistentStoreCoordinator*    persistentStoreCoordinator_;
}

@property (nonatomic, weak) id <ASCDProtocol> delegate;

// Core Data
@property (nonatomic, strong )      NSString                    *dbStoreLocation;
@property (nonatomic, strong, readonly) NSManagedObjectContext  *managedObjectContext;


+ (ASCoreData *)sharedInstance;

- (void) resetMOC;
- (void) clearDatabase; // totally removes all data by deleting the store
- (void) resetDatabase; // Reload database from the db store recreates all db contexts etc.
- (void) saveContext;
- (void) saveContext:(NSManagedObjectContext*)moc;
- (NSManagedObjectContext*) newMOC;
- (void) removeAllEntitiesofType:(NSString*) entityName;
- (void) resetPersistentStoreCoordinator:(NSTimer*) timer;

//Queries

- (NSArray*) entitiesofType:(NSString*)entType
                 filteredBy:(NSPredicate*)filter
                   sortedBy:(NSArray*)sortKeys
                  ascending:(BOOL)direction
                      inMoc:(NSManagedObjectContext*)localMOC;

-(NSArray*)findEntitiesByUpdateFlag:(BOOL) updateStatus
                             ofKind:(NSString*)entityName
                              inMOC:(NSManagedObjectContext*)localMOC;


- (NSMutableArray*) copyAllEntitiesofType:(NSString*)entType inMOC:(NSManagedObjectContext*)moc sortedBy:(NSString*)sortKey ascending:(BOOL)direction;

- (NSMutableArray*) copyAllEntitiesofType:(NSString*)entType sortedBy:(NSString*)sortKey ascending:(BOOL)direction;


// Utils
-(void)removeDemodata;

-(void) listAllEntitiesofType:(NSString*) entityName inMOC:(NSManagedObjectContext*)moc;

-(NSInteger) countAllEntitiesofType:(NSString*) entityName
                              inMOC:(NSManagedObjectContext*)moc;

-(NSInteger) countAllModifedEntitiesofType:(NSString*) entityName inMOC:(NSManagedObjectContext*)moc;
-(NSInteger) uploadCount:(NSString*) entityName inMOC:(NSManagedObjectContext*)moc;


// Creates



-(DBObject*)findorCreateEntityByUUID:(NSString*) targetUUID
                                ofType:(NSString*)entityType;

//Deletes
-(void) deleteDBObject:(DBObject*)deleteTarget;

// Queries

-(NSArray*)findDBObjectsOfKind:(NSString*)dbObjectKind
                      starting:(NSDate*)startDate
                        ending:(NSDate*)endDate
                          sort:(NSArray*)sortDescriptors;

-(ASSource*)  getSourceByURL:(NSString*)target;
-(ASUser*)   getUserByName:(NSString*)target;
-(ASMarkup*)   getMarkupFor:(NSString*)sourceURL byUser:(NSString*)userName;
-(ASCategory*) categoryByName:(NSString*)catName;


// Handle the store itself
-(NSURL*)getDBStoreasURL;
-(void) replaceDBStoreWithData:(NSData*)dbContent;

@end

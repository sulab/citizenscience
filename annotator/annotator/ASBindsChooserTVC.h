//
//  ASBindsChooserTVC.h
//  annotator
//
//  Created by Richard Good on 11/9/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASCategory.h"
#import "ASBind.h"

@protocol BindProtocol <NSObject>

-(void) addBind:(ASBind*)bind;

@end

@interface ASBindsChooserTVC : UITableViewController
@property (nonatomic, weak) id <BindProtocol> delegate;


-(void)showPossibleBindsFor:(NSString*)source withDestination:(NSString*)destination;
@end

//
//  AppDelegate.h
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASUser.h"
#import "ASSource.h"
#import "ASMarkup.h"

@interface AnnotatorAD : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ASUser*   currentUser;
@property (strong, nonatomic) ASMarkup* currentMarkup;
@property (strong, nonatomic) ASSource* currentSource;


@end


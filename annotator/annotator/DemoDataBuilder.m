//
//  DemoDataBuilder.m
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "DemoDataBuilder.h"
#import "ASCategory.h"

#import "ASMarkup.h"
#import "ASCoreData.h"
#import "GTConstants.h"
#import "ASBind.h"

@implementation DemoDataBuilder{
    ASCategory* geneCat;
    ASCategory* diseaseCat;
    ASCategory* drugCat;
}

-(ASUser*) insertUser{
    ASUser* user = (ASUser*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:UserEntityname];
    user.name = @"TestUser";
    user.kind = @"Human";
    return user;
}

-(void) insertSources{
    ASSource* source = (ASSource*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:SourceEntityname];
    source.url = @"Test 1";
    source.content = @"Rationale for co-targeting IGF-1R and ALK in ALK fusion-positive lung cancer. Crizotinib, a selective tyrosine kinase inhibitor (TKI), shows marked activity in patients whose lung cancers harbor fusions in the gene encoding anaplastic lymphoma receptor tyrosine kinase (ALK), but its efficacy is limited by variable primary responses and acquired resistance. In work arising from the clinical observation of a patient with ALK fusion-positive lung cancer who had an exceptional response to an insulin-like growth factor 1 receptor (IGF-1R)-specific antibody, we define a therapeutic synergism between ALK and IGF-1R inhibitors. Similar to IGF-1R, ALK fusion proteins bind to the adaptor insulin receptor substrate 1 (IRS-1), and IRS-1 knockdown enhances the antitumor effects of ALK inhibitors. In models of ALK TKI resistance, the IGF-1R pathway is activated, and combined ALK and IGF-1R inhibition improves therapeutic efficacy. Consistent with this finding, the levels of IGF-1R and IRS-1 are increased in biopsy samples from patients progressing on crizotinib monotherapy. Collectively these data support a role for the IGF-1R-IRS-1 pathway in both ALK TKI-sensitive and ALK TKI-resistant states and provide a biological rationale for further clinical development of dual ALK and IGF-1R inhibitors.";

    source = (ASSource*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:SourceEntityname];
    source.url = @"Test 2";
    source.content =@"Alopecia areata is driven by cytotoxic T lymphocytes and is reversed by JAK inhibition. Alopecia areata (AA) is a common autoimmune disease resulting from damage of the hair follicle by T cells. The immune pathways required for autoreactive T cell activation in AA are not defined limiting clinical development of rational targeted therapies. Genome-wide association studies (GWAS) implicated ligands for the NKG2D receptor (product of the KLRK1 gene) in disease pathogenesis. Here, we show that cytotoxic CD8(+)NKG2D(+) T cells are both necessary and sufficient for the induction of AA in mouse models of disease. Global transcriptional profiling of mouse and human AA skin revealed gene expression signatures indicative of cytotoxic T cell infiltration, an interferon-γ (IFN-γ) response and upregulation of several γ-chain (γc) cytokines known to promote the activation and survival of IFN-γ-producing CD8(+)NKG2D(+) effector T cells. Therapeutically, antibody-mediated blockade of IFN-γ, interleukin-2 (IL-2) or interleukin-15 receptor β (IL-15Rβ) prevented disease development, reducing the accumulation of CD8(+)NKG2D(+) T cells in the skin and the dermal IFN response in a mouse model of AA. Systemically administered pharmacological inhibitors of Janus kinase (JAK) family protein tyrosine kinases, downstream effectors of the IFN-γ and γc cytokine receptors, eliminated the IFN signature and prevented the development of AA, while topical administration promoted hair regrowth and reversed established disease. Notably, three patients treated with oral ruxolitinib, an inhibitor of JAK1 and JAK2, achieved near-complete hair regrowth within 5 months of treatment, suggesting the potential clinical utility of JAK inhibition in human AA.";
 
    source = (ASSource*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:SourceEntityname];
    source.url = @"Test 3";
    source.content =@"Novel thiazole amine class tyrosine kinase inhibitors induce apoptosis in human mast cells expressing D816V KIT mutation. Gain-of-function mutations of receptor tyrosine kinase KIT play a critical role in the pathogenesis of systemic mastocytosis (SM) and gastrointestinal stromal tumors. D816V KIT mutation, found in ∼80% of SM, is resistant to the currently available tyrosine kinase inhibitors (TKIs) (e.g. imatinib mesylate). Therefore, development of promising TKIs for the treatment of D816V KIT mutation is still urgently needed. We synthesized thiazole amine compounds and chose one representative designated 126332 to investigate its effect on human mast cells expressing KIT mutations. We found 126332 inhibited the phosphorylation of KIT and its downstream signaling molecules Stat3 and Stat5. 126332 inhibited the proliferation of D816V KIT expressing cells. 126332 induced apoptosis and downregulated levels of Mcl-1 and survivin. Furthermore, 126332 inhibited the tyrosine phosphorylation of β-catenin, inhibited β-catenin-mediated transcription and DNA binding of TCF. Moreover, 126332 also exhibited in vivo antineoplastic activity against cells harboring D816V mutation. Our findings suggest thiazole amine compounds may be promising agents for the treatment of diseases caused by KIT mutation.";

    source = (ASSource*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:SourceEntityname];
    source.url = @"Test 4";
    source.content =@"DNA-damage-induced differentiation of leukaemic cells as an anti-cancer barrier. Self-renewal is the hallmark feature both of normal stem cells and cancer stem cells. Since the regenerative capacity of normal haematopoietic stem cells is limited by the accumulation of reactive oxygen species and DNA double-strand breaks, we speculated that DNA damage might also constrain leukaemic self-renewal and malignant haematopoiesis. Here we show that the histone methyl-transferase MLL4, a suppressor of B-cell lymphoma, is required for stem-cell activity and an aggressive form of acute myeloid leukaemia harbouring the MLL-AF9 oncogene. Deletion of MLL4 enhances myelopoiesis and myeloid differentiation of leukaemic blasts, which protects mice from death related to acute myeloid leukaemia. MLL4 exerts its function by regulating transcriptional programs associated with the antioxidant response. Addition of reactive oxygen species scavengers or ectopic expression of FOXO3 protects MLL4(-/-) MLL-AF9 cells from DNA damage and inhibits myeloid maturation. Similar to MLL4 deficiency, loss of ATM or BRCA1 sensitizes transformed cells to differentiation, suggesting that myeloid differentiation is promoted by loss of genome integrity. Indeed, we show that restriction-enzyme-induced double-strand breaks are sufficient to induce differentiation of MLL-AF9 blasts, which requires cyclin-dependent kinase inhibitor p21(Cip1) (Cdkn1a) activity. In summary, we have uncovered an unexpected tumour-promoting role of genome guardians in enforcing the oncogene-induced differentiation blockade in acute myeloid leukaemia.";

    source = (ASSource*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:SourceEntityname];
    source.url = @"Test 5";
    source.content =@"EGF receptor uses SOS1 to drive constitutive activation of NFκB in cancer cells. Activation of nuclear factor κB (NFκB) is a central event in the responses of normal cells to inflammatory signals, and the abnormal constitutive activation of NFκB is important for the survival of most cancer cells. In nonmalignant human cells, EGF stimulates robust activation of NFκB. The kinase activity of the EGF receptor (EGFR) is required, because the potent and specific inhibitor erlotinib blocks the response. Down-regulating EGFR expression or inhibiting EGFR with erlotinib impairs constitutive NFκB activation in several different types of cancer cells and, conversely, increased activation of NFκB leads to erlotinib resistance in these cells. We conclude that EGF is an important mediator of NFκB activation in cancer cells. To explore the mechanism, we selected an erlotinib-resistant cell line in which the guanine nucleotide exchange factor Son of Sevenless 1 (SOS1), well known to be important for EGF-dependent signaling to MAP kinases, is overexpressed. Increased expression of SOS1 increases NFκB activation in several different types of cancer cells, and ablation of SOS1 inhibits EGF-induced NFκB activation in these cells, indicating that SOS1 is a functional component of the pathway connecting EGFR to NFκB activation. Importantly, the guanine nucleotide exchange activity of SOS1 is not required for NFκB activation.";

    source = (ASSource*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:SourceEntityname];
    source.url = @"Test 6";
    source.content =@"The pleiotropic profile of the indirubin derivative 6BIO overcomes TRAIL resistance in cancer. TRAIL (TNFα-related apoptosis-inducing factor) has been promoted as a promising anti-cancer agent. Unfortunately many tumor cells develop resistance towards TRAIL due to numerous defects in apoptotic signaling. To handle this problem combination therapy with compounds affecting as many different anti-apoptotic targets as possible might be a feasible approach. The bromo-substituted indirubin derivative 6BIO meets this challenge: Treatment of breast cancer and bladder carcinoma cell lines with micromolar concentrations of 6BIO abrogates cellular growth and induces apoptosis. Combination of subtoxic amounts of 6BIO with ineffective doses of TRAIL completely abolishes proliferation and long-term survival of cancer cells. As shown in two-dimensional as well as three-dimensional cell culture models, 6BIO potently augments TRAIL-induced apoptosis in cancer cell lines. The potent chemosensitizing effect of 6BIO to TRAIL-mediated cell death is due to the pleiotropic inhibitory profile of 6BIO. As shown previously, 6BIO abrogates STAT3, PDK1 as well as GSK3 signaling and moreover, inhibits the expression of the anti-apoptotic Bcl-2 family members Bcl-xL and Mcl-1 on mRNA as well as on protein level, as demonstrated in this study. Moreover, the expression of cFLIP and cIAP1 is significantly downregulated in 6BIO treated cancer cell lines. In sum (subtoxic concentration of) the multi-kinase inhibitor 6BIO serves as a potent chemosensitizing agent fighting TRAIL resistant cancer cells.";

    source = (ASSource*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:SourceEntityname];
    source.url = @"Test 7";
    source.content =@"Loss of oncogenic Notch1 with resistance to a PI3K inhibitor in T-cell leukaemia. Mutations that deregulate Notch1 and Ras/phosphoinositide 3 kinase (PI3K)/Akt signalling are prevalent in T-cell acute lymphoblastic leukaemia (T-ALL), and often coexist. Here we show that the PI3K inhibitor GDC-0941 is active against primary T-ALLs from wild-type and Kras(G12D) mice, and addition of the MEK inhibitor PD0325901 increases its efficacy. Mice invariably relapsed after treatment with drug-resistant clones, most of which unexpectedly had reduced levels of activated Notch1 protein, downregulated many Notch1 target genes, and exhibited cross-resistance to γ-secretase inhibitors. Multiple resistant primary T-ALLs that emerged in vivo did not contain somatic Notch1 mutations present in the parental leukaemia. Importantly, resistant clones upregulated PI3K signalling. Consistent with these data, inhibiting Notch1 activated the PI3K pathway, providing a likely mechanism for selection against oncogenic Notch1 signalling. These studies validate PI3K as a therapeutic target in T-ALL and raise the unexpected possibility that dual inhibition of PI3K and Notch1 signalling could promote drug resistance in T-ALL.";

    source = (ASSource*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:SourceEntityname];
    source.url = @"Test 8";
    source.content =@"Restoration of miR-193b sensitizes Hepatitis B virus-associated hepatocellular carcinoma to sorafenib. BACKGROUND: Chronic infection with Hepatitis B virus (HBV) is the major risk factor of Hepatocellular Carcinoma (HCC). This study is to explore the mechanism of sorafenib resistance and find an effective strategy to sensitize HBV-associated HCC to sorafenib. METHODS: Cytotoxicity to sorafenib was evaluated in HBV-positive/negative HCC cell lines. Expression of miR-193b and myeloid cell leukemia-1 (Mcl-1) protein were assessed by Q-PCR, in situ hybridization and western blot, immunohistochemistry, respectively. A luciferase reporter of Mcl-1 3'-UTR was used for validation as a target of miR-193b. Cell apoptosis was measured by flow cytometry, caspase-3 activity assay and DAPI staining. RESULT: The IC50 to sorafenib was significantly higher in HBV-positive HCC cells than those without HBV infection. Significant downregulation of miR-193b and a higher level of Mcl-1 were observed in HBV-positive HCC cells and tissues. The activity of Mcl-1 3'-UTR reporter was inhibited by co-transfection with miR-193b mimic. Restoring the expression of miR-193b sensitized HBV-associated HCC cells to sorafenib treatment and facilitated sorafenib-induced apoptosis. CONCLUSIONS: Modulation of miRNAs expression might be a potential way to enhance response to sorafenib in HBV-associated HCC.";

    source = (ASSource*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:SourceEntityname];
    source.url = @"Test 9";
    source.content =@"Synergistic tumor suppression by combined inhibition of telomerase and CDKN1A. Tumor suppressor p53 plays an important role in mediating growth inhibition upon telomere dysfunction. Here, we show that loss of the p53 target gene cyclin-dependent kinase inhibitor 1A (CDKN1A, also known as p21(WAF1/CIP1)) increases apoptosis induction following telomerase inhibition in a variety of cancer cell lines and mouse xenografts. This effect is highly specific to p21, as loss of other checkpoint proteins and CDK inhibitors did not affect apoptosis. In telomerase, inhibited cell loss of p21 leads to E2F1- and p53-mediated transcriptional activation of p53-upregulated modulator of apoptosis, resulting in increased apoptosis. Combined genetic or pharmacological inhibition of telomerase and p21 synergistically suppresses tumor growth. Furthermore, we demonstrate that simultaneous inhibition of telomerase and p21 also suppresses growth of tumors containing mutant p53 following pharmacological restoration of p53 activity. Collectively, our results establish that inactivation of p21 leads to increased apoptosis upon telomerase inhibition and thus identify a genetic vulnerability that can be exploited to treat many human cancers containing either wild-type or mutant p53.";

    source = (ASSource*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:SourceEntityname];
    source.url = @"Test 10";
    source.content =@"Anticancer activity of HS-527, a novel inhibitor targeting PI3-kinase in human pancreatic cancer cells. Pancreatic cancer is known to have low 5-year survival rate and poor response to treatment. In this study, we synthesized HS-527, a new PI3-kinase inhibitor, and investigated not only its anticancer activity, but also its mechanism of action in pancreatic cancer cells. HS-527 had higher specificity for PI3K than other kinases and inhibited PI3K/Akt signaling pathway by down-regulating Akt and P70S6K. And HS-527 inhibited the cell growth and proliferation of the pancreatic cancer in a time- and dose-dependent manner, with greater activity than gemcitabine. Even HS-527 showed lower cytotoxicity than gemcitabine in normal cells. When treated with HS-527, the cancer cells appeared apoptotic, increasing the expression of cleaved PARP, cleaved caspase-3, and Bax. Furthermore, HS-527 showed an anti-angiogenic activity by decreasing the expression of HIF-1α and VEGF, and inhibited the migration of endothelial cells, and the formation of new blood vessel in mouse Matrigel plug assay. In this study, we found that HS-527 showed anti-cancer activity through an inhibition of the PI3K/Akt pathway in pancreatic cancer cells, suggesting that HS-527 could be used as a promising therapeutic agent for pancreatic cancer.";

}

-(void) insertCategories{
    geneCat = (ASCategory*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:CategoryEntityname];
    geneCat.name = @"Gene";
    diseaseCat = (ASCategory*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:CategoryEntityname];
    diseaseCat.name = @"Disease";
    drugCat = (ASCategory*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:CategoryEntityname];
    drugCat.name = @"Drug";
}

-(void) insertBind:(NSString*)name source:(ASCategory*)source destination:(ASCategory*)destination{
    ASBind* bind = (ASBind*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:BindEntityName];
    bind.name =name;
    bind.sourceCategory = source;
    bind.destinationCategory = destination;
 
}

-(void) insertBinds{
    [self insertBind:@"physically interacts with" source:geneCat  destination:geneCat];
    [self insertBind:@"chemically modifies" source:geneCat  destination:geneCat];
    [self insertBind:@"coregulated or coexpressed" source:geneCat  destination:geneCat];
    [self insertBind:@"regulates" source:geneCat  destination:geneCat];
    [self insertBind:@"activates" source:geneCat  destination:geneCat];
    [self insertBind:@"inhibits" source:geneCat  destination:geneCat];

    [self insertBind:@"mutation causes" source:geneCat  destination:diseaseCat];
    [self insertBind:@"overexpression/activation causes" source:geneCat  destination:diseaseCat];
    [self insertBind:@"underexpression/repression causes" source:geneCat  destination:diseaseCat];
    [self insertBind:@"is a biomarker of" source:geneCat  destination:diseaseCat];
    [self insertBind:@"is caused by mutation of" source:diseaseCat  destination:geneCat];
    [self insertBind:@"is caused by overexpression/activation of" source:diseaseCat  destination:geneCat];
    [self insertBind:@"is caused by underexpression/repression of" source:diseaseCat  destination:geneCat];
    [self insertBind:@"has biomarker" source:diseaseCat  destination:geneCat];


    [self insertBind:@"is targeted by" source:geneCat  destination:drugCat];
    [self insertBind:@"metabolizes" source:geneCat  destination:drugCat];
    [self insertBind:@"mutation increases sensitivity" source:geneCat  destination:drugCat];
    [self insertBind:@"mutation decreases sensitivity" source:geneCat  destination:drugCat];
    [self insertBind:@"targets" source:drugCat  destination:geneCat];
    [self insertBind:@"is metabolized by" source:drugCat  destination:geneCat];
    [self insertBind:@"is more effective due to mutation in" source:drugCat  destination:geneCat];
    [self insertBind:@"is less effective due to mutation in" source:drugCat  destination:geneCat];

    [self insertBind:@"co-occurs with" source:diseaseCat  destination:diseaseCat];
    
    [self insertBind:@"is treated by" source:diseaseCat  destination:drugCat];
    [self insertBind:@"is caused by" source:diseaseCat  destination:drugCat];
    [self insertBind:@"is aggravated by" source:diseaseCat  destination:drugCat];
    [self insertBind:@"is treatment for" source:drugCat  destination:diseaseCat];
    [self insertBind:@"causes" source:drugCat  destination:diseaseCat];
    [self insertBind:@"aggravates" source:drugCat  destination:diseaseCat];

    
    [self insertBind:@"duplicates effect" source:drugCat  destination:drugCat];
    [self insertBind:@"alters efficiency" source:drugCat  destination:drugCat];
    [self insertBind:@"antagonizes" source:drugCat  destination:drugCat];

}


-(void) initData{
    ASUser* user  = [self insertUser];
    [self insertSources];
    [self insertCategories];
    [self insertBinds];
    ASMarkup* markup =(ASMarkup*)[[ASCoreData sharedInstance] findorCreateEntityByUUID:nil ofType:MarkupEntityname];
    ASSource * source = [[ASCoreData sharedInstance] getSourceByURL:@"Test 1"];
    [source addHasMarkupsObject:markup];
    markup.content = source.content;
    markup.enteredBy = user;
}

@end

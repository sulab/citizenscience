//
//  GTConstants.m
//  trueeight
//
//  Created by Richard Good on 8/14/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "GTConstants.h"

@implementation GTConstants

// Database Entities
NSString * const DBObjectEntityname = @"DBObject";

NSString * const        BindEntityName = @"ASBind";
NSString * const		ConceptEntityname = @"ASConcept";
NSString * const        UserEntityname  = @"ASUser";
NSString * const        MarkupEntityname = @"ASMarkup";
NSString * const        SourceEntityname = @"ASSource";
NSString * const        CategoryEntityname = @"ASCategory";
NSString * const        ConceptBindEntityname = @"ASConceptBind";

@end

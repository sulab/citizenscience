//
//  GTConstants.h
//  trueeight
//
//  Created by Richard Good on 8/14/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTConstants : NSObject

// Database Entities
extern  NSString * const        DBObjectEntityname;

extern	NSString * const        BindEntityName;
extern	NSString * const		ConceptEntityname;
extern	NSString * const        UserEntityname;
extern	NSString * const        MarkupEntityname;
extern	NSString * const        SourceEntityname;
extern	NSString * const        CategoryEntityname;
extern	NSString * const        ConceptBindEntityname;

@end

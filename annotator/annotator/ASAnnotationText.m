//
//  ASAnnotationText.m
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "ASAnnotationText.h"

@implementation ASAnnotationText

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void) addConcept:(NSString*)catKind{
    NSRange selectedRg = self.selectedRange;
    
    NSString * content  = [self.text  substringWithRange:selectedRg];
    if ([self.conceptDelegate respondsToSelector:@selector(insertAConcept:forKind: atLocation:)])
    {
        [self.conceptDelegate insertAConcept:content forKind:catKind atLocation:selectedRg];
    }
   
}

-(void) addGene:(id)sender{
    [self addConcept:@"Gene"];
}

-(void) addDrug:(id)sender{
    [self addConcept:@"Drug"];
}

-(void) addDisease:(id)sender{
    [self addConcept:@"Disease"];   
}


- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [self setMenuUp];
    if (action == @selector(addGene:)  || action == @selector(addDrug:) || action == @selector(addDisease:))
        return YES;
    else
        return NO;
}

-(void) setMenuUp{
    UIMenuItem *menuItemGene = [[UIMenuItem alloc] initWithTitle:@"Gene" action:@selector(addGene:)];
    UIMenuItem *menuItemDrug = [[UIMenuItem alloc] initWithTitle:@"Drug" action:@selector(addDrug:)];
    UIMenuItem *menuItemDisease = [[UIMenuItem alloc] initWithTitle:@"Disease" action:@selector(addDisease:)];
    UIMenuController *menuCont = [UIMenuController sharedMenuController];
    menuCont.menuItems = [NSArray arrayWithObjects:menuItemGene,menuItemDrug,menuItemDisease, nil];
}


@end

//
//  ASConceptBind.h
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DBObject.h"

@class ASConcept,ASBind;

@interface ASConceptBind : DBObject

@property (nonatomic, retain) ASConcept *sourceBind;
@property (nonatomic, retain) ASConcept *destinationBind;
@property (nonatomic, retain) ASBind *isA;

@end

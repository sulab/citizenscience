//
//  ASMarkup.h
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DBObject.h"

@class ASConcept, ASSource, ASUser;

@interface ASMarkup : DBObject

@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) ASSource *isFor;
@property (nonatomic, retain) ASUser *enteredBy;
@property (nonatomic, retain) ASConcept *contains;

@end

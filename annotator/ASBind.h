//
//  ASBind.h
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DBObject.h"

@class ASCategory, ASConceptBind;

@interface ASBind : DBObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *usedBy;
@property (nonatomic, retain) ASCategory *sourceCategory;
@property (nonatomic, retain) ASCategory *destinationCategory;
@end

@interface ASBind (CoreDataGeneratedAccessors)

- (void)addUsedByObject:(ASConceptBind *)value;
- (void)removeUsedByObject:(ASConceptBind *)value;
- (void)addUsedBy:(NSSet *)values;
- (void)removeUsedBy:(NSSet *)values;

@end

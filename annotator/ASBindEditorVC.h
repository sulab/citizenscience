//
//  ASBindEditorVC.h
//  annotator
//
//  Created by Richard Good on 11/9/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASBind.h"

@interface ASBindEditorVC : UIViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *sourceCategory;
@property (weak, nonatomic) IBOutlet UISegmentedControl *destinationCategory;
@property (weak, nonatomic) IBOutlet UITextField *bindName;
@property (strong,nonatomic) ASBind * bind;
@end

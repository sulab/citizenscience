//
//  ASCategory.m
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "ASCategory.h"


@implementation ASCategory

@dynamic name;
@dynamic isSourceFor;
@dynamic isDestinationFor;
@dynamic superClassOf;

@end

//
//  ASConcept.m
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "ASConcept.h"
#import "ASCategory.h"
#import "ASConceptBind.h"
#import "ASMarkup.h"


@implementation ASConcept

@dynamic start_pos;
@dynamic length;
@dynamic text_attributes;
@dynamic name;
@dynamic foundIn;
@dynamic hasSourceBind;
@dynamic hasDestinationBind;
@dynamic kindOf;

@end

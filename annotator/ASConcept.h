//
//  ASConcept.h
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DBObject.h"

@class ASCategory, ASConceptBind, ASMarkup;

@interface ASConcept : DBObject

@property (nonatomic, retain) NSNumber * start_pos;
@property (nonatomic, retain) NSNumber * length;
@property (nonatomic, retain) NSString * text_attributes;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) ASMarkup *foundIn;
@property (nonatomic, retain) ASConceptBind *hasSourceBind;
@property (nonatomic, retain) ASConceptBind *hasDestinationBind;
@property (nonatomic, retain) ASCategory *kindOf;

@end

//
//  ASUser.h
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DBObject.h"


@interface ASUser : DBObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * kind;
@property (nonatomic, retain) NSSet *hasMarkups;
@end

@interface ASUser (CoreDataGeneratedAccessors)

- (void)addHasMarkupsObject:(NSManagedObject *)value;
- (void)removeHasMarkupsObject:(NSManagedObject *)value;
- (void)addHasMarkups:(NSSet *)values;
- (void)removeHasMarkups:(NSSet *)values;

@end

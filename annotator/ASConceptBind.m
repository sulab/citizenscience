//
//  ASConceptBind.m
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "ASConceptBind.h"


@implementation ASConceptBind

@dynamic sourceBind;
@dynamic destinationBind;
@dynamic isA;

@end

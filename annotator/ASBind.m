//
//  ASBind.m
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "ASBind.h"
#import "ASCategory.h"
#import "ASConceptBind.h"


@implementation ASBind

@dynamic name;
@dynamic usedBy;
@dynamic sourceCategory;
@dynamic destinationCategory;

@end

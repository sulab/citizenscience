//
//  ASMarkup.m
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import "ASMarkup.h"
#import "ASConcept.h"
#import "ASSource.h"
#import "ASUser.h"


@implementation ASMarkup

@dynamic content;
@dynamic isFor;
@dynamic enteredBy;
@dynamic contains;

@end

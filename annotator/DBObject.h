//
//  DBObject.h
//  annotator
//
//  Created by Richard Good on 11/8/14.
//  Copyright (c) 2014 Richard Good. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DBObject : NSManagedObject

@property (nonatomic, retain) NSString * dbID;
@property (nonatomic, retain) NSDate * create_date;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic, retain) NSNumber * uploaded;
@property (nonatomic, retain) NSNumber * tobeDeleted;


-(void) initValues;

@end
